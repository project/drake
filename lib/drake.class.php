<?php

/**
 * Drake class file.
 *
 * Class that offers Drake callables.
 *
 * @filesource
 * @link			http://dev.sypad.com/projects/drake Drake
 * @package			drake
 * @subpackage		drupal.lib
 * @since			1.0
 */

/**
 * Drake Callables.
 * 
 * @author		Mariano Iglesias - mariano@cricava.com
 * @package		drake
 * @subpackage	drupal.lib
 */
class Drake
{
	/**#@+
	 * @access private
	 */
	/**
	 * Drupal's root URL.
	 * 
	 * @since 1.0
	 * @var string
	 */
	var $drupalUrl;
	
	/**
	 * Current CakePHP application identifier.
	 * 
	 * @since 1.0
	 * @var string
	 */
	var $application;
	/**#@-*/
	
	/**
	 * Singleton implementation (PHP4/PHP5)
	 *
	 * @return mixed	A shared instance
	 * 
	 * @access public
	 * @static
	 * @since 1.0
	 */
	function &getInstance()
	{
		static $instances = array();
		
		$className = __CLASS__;
		
		if (!isset($instances[$className]))
		{
			$instances[$className] = new $className();
		}
		
		return $instances[$className];
	}

	function Object()
	{
		$args = func_get_args();
		
		if (method_exists($this, '__destruct'))
		{
			register_shutdown_function(array(&$this, '__destruct'));
		}

		call_user_func_array(array(&$this, '__construct'), $args);
	}

	function __construct()
	{
	}
	
	/**
	 * Set Drupal's URL.
	 * 
	 * @param string $drupalUrl	Drupal's URL
	 * 
	 * @access public
	 * @since 1.0
	 */
	function setDrupalUrl($drupalUrl)
	{
		$this->drupalUrl = $drupalUrl;
	}
	
	/**
	 * Set current CakePHP application.
	 * 
	 * @param string $application	Current application
	 * 
	 * @access public
	 * @since 1.0
	 */
	function setApplication($application)
	{
		$this->application = $application;
	}
	
	/**
	 * Returns if CakePHP application is being run on Drake.
	 *
	 * @return bool	true if it is running on Drake, false otherwise
	 * 
	 * @access public
	 * @since 1.0
	 */
	function isDrake()
	{
		return defined('DRAKE');
	}
	
	/**
	 * Gets the URL to execute the specified CakePHP action with Drake
	 *
	 * @param string $action	CakePHP URL. (leave empty for default cake action)
	 * @param string $application	The identifier of the application in the configuration file (leave empty for default)
	 *
	 * @return string	A valid Drake URL
	 *
	 * @access public
	 * @since 1.0
	 */
	function getUrl($action = null, $application = null)
	{
		if ($this->isDrake() && isset($this->cakeApplicationId))
		{
			$application = $this->cakeApplicationId;
		}
		
		$url = $this->drupalUrl . (isset($application) ? '/' . $application : '');
		
		if (isset($action))
		{
			$url .= (strpos($url, '?') !== false ? '&' : '?');
			$url .= 'run=' . urlencode($action);
		}
		
		return $url;
	}
}

?>